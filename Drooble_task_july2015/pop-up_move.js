function iframeSrc(newAddress) {
	var add = "m/v";
	var end = "&autoplay=1";
	var newIframeSrc = "";
	for (var i = 0; i < newAddress.length; i++) {
		if (newAddress.charCodeAt(i) == 109) {
		newIframeSrc = newAddress.replace(newAddress.charAt(i), add);
		newIframeSrc = newIframeSrc + end;
		break;
		}
	}
	return newIframeSrc;
};		
$(document).ready(function() {
	$("#pop-up").hide();
	$("#show-pop-up").click(function() {
		$("#pop-up").show();
		$("#show-pop-up").hide();
	})
	$("#newURL").keypress(function(event) {

		var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == 13) {
	    	$("#no-video-for-play").hide();
		    $("#iframeId").attr('src', iframeSrc($("#newURL").val()));
		    $("#newURL").val("");
		}
		    event.stopPropagation();
	});
});
